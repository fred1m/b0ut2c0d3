#!/bin/bash

# merci `shellcheck` https://www.shellcheck.net/wiki/SC2086

cd ~/p35 || exit # pour crontab (https://www.shellcheck.net/wiki/SC2164)
dossier=4CCFE4et5	# 1/3 CIBLE (`mkdir` automatique)
orig=/public_html/	# 2/3 Source (dossier)
lesfic='E4/TablSynth.pdf E5/FicheRP1.pdf E5/FicheRP2.pdf'	# 3/3 liste de fichier(s)
pourvoir=~/public_html/p24/$dossier.html # ~~autogestion~~
lespseudo=p35liste.4nn # liste confidentielle
promo=p35
log=$dossier/ramasser_$lespseudo.log # au format MarckDown pour pandoc
if [[ -d $dossier ]]
 then
 echo "Le dossier \"$dossier\" existe." > /dev/null
else
 if mkdir $dossier
  then
  echo "Le dossier $dossier à été créé."
  else
  echo "Échec mkdir $dossier !"
  exit 1
 fi
fi
# voir http://abs.traduc.org/abs-fr/ch14.html#readredir
exec 6>&1           # Lie le descripteur de fichier #6 avec stdout.
				 # Sauvegarde stdout.
exec > $log         # stdout remplacé par le fichier...
echo "% Évaluation de la livraison de"
echo " "
echo "~~~"
echo "$lesfic"
echo "~~~"
echo " dans \`~$orig\` le " ;  date
echo " "
while IFS=, read -r pseudo nom prenom _
do
echo " " ; echo "* **$nom $prenom**"
for fic in $lesfic
	do
	if [[ -f "/skole/tjener/home0/$promo/$pseudo$orig$fic" ]]
	 then
	 ficCible=${fic/\//_} # merci https://abs.traduc.org/abs-fr/ch10.html
		if cp -p "/skole/tjener/home0/$promo/$pseudo$orig$fic" "$dossier/$pseudo$ficCible"
			then
			echo -e "\t* $fic livré"
			echo "~~~"
			pdfinfo $dossier/$pseudo$ficCible | grep -e "Pages" -e "Page size"
			echo "~~~"
			else
			echo -e "\t* $fic *NON COPIÉ*"
		fi
	 else
		echo -e "\t* insuffisant ($fic manquant)"
	fi
	done
done <$lespseudo   # Redirection d'entrées/sorties.
echo " "
echo "---- "
echo " "
echo "~~~"
ls -rtlh $dossier
echo "~~~"
exec 1>&6 6>&-      # Restaure stdout et ferme le descripteur de fichier #6.
pandoc -f markdown --ascii -s -c /~fred/pandoc.css --highlight-style haddock -t html5 -o $pourvoir $log
exit 0
